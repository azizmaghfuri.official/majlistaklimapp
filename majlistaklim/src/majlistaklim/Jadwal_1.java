/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package majlistaklim;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author acer
 */
@Entity
@Table(name = "jadwal", catalog = "majlis_taklim", schema = "")
@NamedQueries({
    @NamedQuery(name = "Jadwal_1.findAll", query = "SELECT j FROM Jadwal_1 j")
    , @NamedQuery(name = "Jadwal_1.findByHari", query = "SELECT j FROM Jadwal_1 j WHERE j.hari = :hari")
    , @NamedQuery(name = "Jadwal_1.findByJamMasuk", query = "SELECT j FROM Jadwal_1 j WHERE j.jamMasuk = :jamMasuk")
    , @NamedQuery(name = "Jadwal_1.findByPelajaran", query = "SELECT j FROM Jadwal_1 j WHERE j.pelajaran = :pelajaran")
    , @NamedQuery(name = "Jadwal_1.findByNamaGuru", query = "SELECT j FROM Jadwal_1 j WHERE j.namaGuru = :namaGuru")})
public class Jadwal_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "Hari")
    private String hari;
    @Basic(optional = false)
    @Column(name = "jam_masuk")
    private String jamMasuk;
    @Id
    @Basic(optional = false)
    @Column(name = "pelajaran")
    private String pelajaran;
    @Basic(optional = false)
    @Column(name = "nama_guru")
    private String namaGuru;

    public Jadwal_1() {
    }

    public Jadwal_1(String pelajaran) {
        this.pelajaran = pelajaran;
    }

    public Jadwal_1(String pelajaran, String hari, String jamMasuk, String namaGuru) {
        this.pelajaran = pelajaran;
        this.hari = hari;
        this.jamMasuk = jamMasuk;
        this.namaGuru = namaGuru;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        String oldHari = this.hari;
        this.hari = hari;
        changeSupport.firePropertyChange("hari", oldHari, hari);
    }

    public String getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(String jamMasuk) {
        String oldJamMasuk = this.jamMasuk;
        this.jamMasuk = jamMasuk;
        changeSupport.firePropertyChange("jamMasuk", oldJamMasuk, jamMasuk);
    }

    public String getPelajaran() {
        return pelajaran;
    }

    public void setPelajaran(String pelajaran) {
        String oldPelajaran = this.pelajaran;
        this.pelajaran = pelajaran;
        changeSupport.firePropertyChange("pelajaran", oldPelajaran, pelajaran);
    }

    public String getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(String namaGuru) {
        String oldNamaGuru = this.namaGuru;
        this.namaGuru = namaGuru;
        changeSupport.firePropertyChange("namaGuru", oldNamaGuru, namaGuru);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pelajaran != null ? pelajaran.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jadwal_1)) {
            return false;
        }
        Jadwal_1 other = (Jadwal_1) object;
        if ((this.pelajaran == null && other.pelajaran != null) || (this.pelajaran != null && !this.pelajaran.equals(other.pelajaran))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "majlistaklim.Jadwal_1[ pelajaran=" + pelajaran + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
